import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Launcher {
  private static final List<Integer> numbers = new ArrayList<>();
  private static final Random random = new Random();
  private static String allowedThreadName = "putter";
  private static int currentSum = 0;
  public static void main(String[] args) {
    Task task = new Task();
    Thread putter = new Thread(task, "putter");
    Thread summer = new Thread(task, "summer");
    Thread rooter = new Thread(task, "rooter");

    putter.start();
    summer.start();
    rooter.start();
  }

  static void determineNextThread(String threadName) {
    switch (threadName) {
      case "putter": allowedThreadName = "summer"; break;
      case "summer": allowedThreadName = "rooter"; break;
      case "rooter": allowedThreadName = "putter"; break;
    }
  }

  private static class Task implements Runnable {

    @Override
    public void run() {
      String currentThreadName = Thread.currentThread().getName();
      while (currentSum < Integer.MAX_VALUE) {
        synchronized (this) {
          if(currentSum < Integer.MAX_VALUE) {
            if (allowedThreadName.equals(currentThreadName)) {
              if (currentThreadName.equals("putter")) {
                int randomInteger = random.nextInt(10) + 1;
                numbers.add(randomInteger);
                System.out.println("added : " + randomInteger);
              } else if (currentThreadName.equals("summer")) {
                currentSum = numbers.stream().mapToInt(Integer::intValue).sum();
                System.out.println("Sum of all numbers: " + currentSum);
              } else if (currentThreadName.equals("rooter")) {
                double sqrt = Math.sqrt(numbers.stream().map(integer ->
                  Math.pow(integer, 2)).mapToDouble(Double::doubleValue).sum());
                System.out.println("square root of sum of squares of all numbers: " + sqrt);
              }
              determineNextThread(currentThreadName);
              this.notifyAll();
            } else {
              try {
                this.wait();
              } catch (InterruptedException e) {
                e.printStackTrace();
              }

              try {
                Thread.sleep(200);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          }
        }
      }
    }
  }
}
