import java.sql.SQLOutput;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.util.LinkedList;
import java.util.function.Consumer;

public class Launcher {
  private final static int CAPACITY = 5;
  public static void main(String[] args) {
    Process process = new Process(CAPACITY);
    Runnable producingTask = () -> {
      try {
        process.produce();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    new Thread(producingTask, "producer-1").start();
    new Thread(producingTask, "producer-2").start();
    new Thread(producingTask, "producer-3").start();

    Runnable consumingTask = () -> {
      try {
        process.consume();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };
    new Thread(consumingTask, "consumer-1").start();
    new Thread(consumingTask, "consumer-2").start();
    new Thread(consumingTask, "consumer-3").start();
  }
}

class Process {
  private final LinkedList<String> messageBus = new LinkedList<>();

  private final int capacity;

  Process(int capacity) {
    this.capacity = capacity;
  }

  public void produce() throws InterruptedException {
    while(true) {
      synchronized(this) {
        while(messageBus.size() == capacity) {
          wait();
        }

        LocalDateTime now = LocalDateTime.now();
        messageBus.add(now.toString());
        System.out.println(Thread.currentThread().getName() + " added, current time: " + now);
        notify();

        Thread.sleep(500);
      }
    }
  }

  public void consume() throws InterruptedException {
    while(true) {
      synchronized(this) {
        while(messageBus.size() == 0) {
          wait();
        }
        String message = messageBus.poll();
        System.out.println(Thread.currentThread().getName() + " retrieved, current time: " + message);
        notify();

        Thread.sleep(500);
      }
    }
  }
}