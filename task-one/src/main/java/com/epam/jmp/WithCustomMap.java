package com.epam.jmp;

import java.util.Map;

public class WithCustomMap {
  public static void main(String[] args) {
    CustomMap numbers = new CustomMap();
    Producer producer = new Producer(numbers);
    Consumer consumer = new Consumer(numbers);
    producer.start();
    consumer.start();
  }

  static class Producer extends Thread {
    private final CustomMap map;

    Producer(CustomMap map) {
      this.map = map;
    }

    @Override
    public void run() {
      int key=0;
      int value=0;
      while (true) {
        map.addValues(key++, value++);
        try {
          sleep(100);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  static class Consumer extends Thread {
    private final CustomMap map;

    Consumer(CustomMap map) {
      this.map = map;
    }

    @Override
    public void run() {
      while (true) {
        try {
          sleep(100);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println("sum: " + map.sum());
      }
    }
  }
}

