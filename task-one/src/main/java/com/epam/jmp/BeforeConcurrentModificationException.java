package com.epam.jmp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BeforeConcurrentModificationException {
  public static void main(String[] args) {
    Map<Integer, Integer> numbers = new HashMap<>();
    Producer producer = new Producer(numbers);
    Consumer consumer = new Consumer(numbers);
    producer.start();
    consumer.start();
  }
}

class Producer extends Thread {
  private final Map<Integer, Integer> numbers;

  public Producer(Map<Integer, Integer> numbers) {
    this.numbers = numbers;
  }

  @Override
  public void run() {
    int key=0;
    int value=0;
    while (true) {
      synchronized (numbers) {
        numbers.put(key++, value++);
      }
      try {
        sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}

class Consumer extends Thread {
  private final Map<Integer, Integer> numbers;

  public Consumer(Map<Integer, Integer> numbers) {
    this.numbers = numbers;
  }

  @Override
  public void run() {
    while (true) {
      int sum = 0;
      int lastUpdateSize=0;
      synchronized (numbers) {
        if(lastUpdateSize != numbers.size()) {
          lastUpdateSize = numbers.size();
          for(Map.Entry<Integer, Integer> entry: numbers.entrySet()) {
            sum += entry.getValue();
          }
        }
      }
      System.out.println("Map size: " + lastUpdateSize + ", sum of elements: " + sum);
      try {
        sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}