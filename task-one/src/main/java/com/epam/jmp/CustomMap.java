package com.epam.jmp;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CustomMap extends HashMap<Integer, Integer> {
  private static int lastUpdateSize = 0;
  private static int lastUpdateValue = 0;
  public Integer sum() {
    if(isEmpty())
      throw new IllegalStateException("Map is empty");
    synchronized (this) {
      lastUpdateValue += get(lastUpdateSize-1);
    }
    return lastUpdateValue;
  }

  public void addValues(Integer key, Integer value) {
    if(value == null) {
      throw new IllegalStateException("Can not insert null value");
    }
    synchronized (this) {
      put(key, value);
      lastUpdateSize++;
    }
  }
}
