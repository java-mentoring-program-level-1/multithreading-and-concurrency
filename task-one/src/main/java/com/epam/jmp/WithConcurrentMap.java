package com.epam.jmp;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class WithConcurrentMap {
  public static void main(String[] args) {
    ConcurrentMap<Integer, Integer> numbers = new ConcurrentHashMap<>();
    WorkerA workerA = new WorkerA(numbers);
    WorkerB workerB = new WorkerB(numbers);
    workerA.start();
    workerB.start();
  }
}

class WorkerA extends Thread {
  private final ConcurrentMap<Integer, Integer> numbers;

  public WorkerA(ConcurrentMap<Integer, Integer> numbers) {
    this.numbers = numbers;
  }

  @Override
  public void run() {
    int key=0;
    int value=0;
    while (true) {
      numbers.put(key++, value++);
      try {
        sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}

class WorkerB extends Thread {
  private final ConcurrentMap<Integer, Integer> numbers;

  public WorkerB(ConcurrentMap<Integer, Integer> numbers) {
    this.numbers = numbers;
  }

  @Override
  public void run() {
    while (true) {
      int sum = 0;
      int lastUpdateSize=0;
      if(lastUpdateSize != numbers.size()) {
        lastUpdateSize = numbers.size();
        for(Map.Entry<Integer, Integer> entry: numbers.entrySet()) {
          sum += entry.getValue();
        }
      }
      System.out.println("Map size: " + lastUpdateSize + ", sum of elements: " + sum);
      try {
        sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
