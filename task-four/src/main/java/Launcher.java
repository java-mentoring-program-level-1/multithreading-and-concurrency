import static java.lang.Thread.sleep;

import java.awt.*;

public class Launcher {
  public static void main(String[] args) {
    BlockingObjectPool pool = new BlockingObjectPool(5);

    Producer producer = new Producer(pool);
    Consumer consumer = new Consumer(pool);
    new Thread(producer, "producer-1").start();
    new Thread(producer, "producer-2").start();

    new Thread(consumer, "consumer-1").start();
    new Thread(consumer, "consumer-2").start();
    new Thread(consumer, "consumer-3").start();
    new Thread(consumer, "consumer-4").start();
  }
}

class Producer implements Runnable {
  private final BlockingObjectPool pool;
  private int value = 0;

  Producer(BlockingObjectPool pool) {
    this.pool = pool;
  }

  @Override
  public void run() {
    do {
      try {
        synchronized (this) {
          value++;
          if (value == Integer.MAX_VALUE) {
            pool.take("q");
            System.out.println(Thread.currentThread().getName() + "quiting...");
            break;
          } else {
            System.out.println(Thread.currentThread().getName() + " produced: " + value);
            pool.take(String.valueOf(value));
          }
        }
        sleep(10000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } while (value < Integer.MAX_VALUE);
  }
}

class Consumer implements Runnable {
  private final BlockingObjectPool pool;

  Consumer(BlockingObjectPool pool) {
    this.pool = pool;
  }

  @Override
  public void run() {
    while (true) {
      String value;
      try {
        value = String.valueOf(pool.get());
        if(value.equals("q")) {
          System.out.println(Thread.currentThread().getName() + " quiting...");
          break;
        }
        System.out.println(Thread.currentThread().getName() + " consumed: " + value);
        sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
